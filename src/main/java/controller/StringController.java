package controller;

import model.StringModel;

/**
 * Is a controller of {@link StringModel} class.
 * Initialize {@link StringModel}.
 */
public class StringController {
    private StringModel sm = StringModel.getInstance();


    /**
     * @return - object of {@link StringModel} singleton class.
     */
    public StringModel getStringModel() {
        return sm;
    }
}
