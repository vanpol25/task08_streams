package first;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Implements functional interface with method that accepts three int values and return int value.
 */
public class FirstTest {

    private static Logger log = LogManager.getLogger(FirstTest.class);

    public void start() {
        FirstTask max = (a, b, c) -> Integer.max(a, Integer.max(b, c));
        FirstTask average = (a, b, c) -> (a + b + c) / 3;

        int maxInt = max.threeValues(5, 7, 2);
        int averageInt = average.threeValues(3, 7, 5);
        log.trace(maxInt);
        log.trace(averageInt);
    }
}
