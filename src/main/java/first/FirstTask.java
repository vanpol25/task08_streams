package first;

@FunctionalInterface
public interface FirstTask {
    int threeValues(int a, int b, int c);
}
