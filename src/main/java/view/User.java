package view;

import controller.StringController;
import model.StringModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

/**
 * Is a view of MVC.
 * Implements singleton pattern.
 */
public class User {

    private static User user;
    private Scanner scan = new Scanner(System.in);
    private StringModel sm = new StringController().getStringModel();
    private Logger log = LogManager.getLogger(User.class);

    private User() {

    }

    public static User getUser() {
        if (user == null) {
            user = new User();
        }
        return user;
    }

    /**
     * Method start.
     */
    public void start() {
        System.out.println("Enter words:");
        do {
            sm.add(scan.nextLine());
        } while (scan.next().equals(""));
        log.info(sm.getText());
        log.info(sm.getNumberOfUnique());
        log.info(sm.getSortedListOfUnique());
        log.info(sm.getWordsNumber());
        log.info(sm.getLettersNumber());
    }

}
