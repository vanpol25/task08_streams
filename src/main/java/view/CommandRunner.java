package view;

import command.Command;
import command.Database;
import command.DeleteCommand;
import command.Developer;

import java.util.Scanner;

/**
 * Implements pattern Command. Each command has its name (with which it is invoked) and one string argument.
 * Implements 4 commands with next ways: command as lambda function, as method reference,
 * as anonymous class, as object of command class. User enters command name and argument into console,
 * app invokes corresponding command.
 */
public class CommandRunner {
    /**
     * Method start.
     */
    public  void start() {
        Database database = new Database();
        Developer developer = new Developer(
                s -> database.insert(s),
                database::update,
                new Command() {
                    @Override
                    public void execute(String s) {
                        database.select(s);
                    }
                },
                new DeleteCommand(database)
        );
        Scanner sc = new Scanner(System.in);
        boolean sw = true;
        do {
            String next = sc.next();
            switch (next) {
                case "insert": {
                    developer.insertData(next);
                    break;
                }
                case "update": {
                    developer.updateData(next);
                    break;
                }
                case "select": {
                    developer.selectData(next);
                    break;
                }
                case "delete": {
                    developer.deleteData(next);
                    break;
                }
                default: {
                    sw = false;
                    break;
                }
            }
        } while (sw);
    }
}
