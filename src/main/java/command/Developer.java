package command;

/**
 * Invoker of command pattern.
 */
public class Developer {
    private Command insert;
    private Command update;
    private Command select;
    private Command delete;

    public Developer(Command insert, Command update, Command select, Command delete) {
        this.insert = insert;
        this.update = update;
        this.select = select;
        this.delete = delete;
    }

    public void insertData(String s) {
        insert.execute(s);
    }
    public void updateData(String s) {
        update.execute(s);
    }
    public void selectData(String s) {
        select.execute(s);
    }
    public void deleteData(String s) {
        delete.execute(s);
    }
}
