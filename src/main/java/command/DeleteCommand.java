package command;

/**
 * Command pattern class.
 * Works with deleting data.
 */
public class DeleteCommand implements Command {
    private Database database;

    public DeleteCommand(Database database) {
        this.database = database;
    }

    @Override
    public void execute(String s) {
        database.delete(s);
    }
}
