package command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * You can do a simple operation with database.
 */
public class Database {

    private Logger log = LogManager.getLogger(Database.class);

    /**
     * @param s - string that will be inserted.
     */
    public void insert(String s) {
        log.info(s.concat(" data..."));
    }

    /**
     * @param s - string that will be updated.
     */
    public void update(String s) {
        log.info(s.concat(" data..."));
    }

    /**
     * @param s - string that will be selected.
     */
    public void select(String s) {
        log.info(s.concat(" data..."));
    }

    /**
     * @param s - string that will be deleted.
     */
    public void delete(String s) {
        log.info(s.concat(" data..."));
    }

}
