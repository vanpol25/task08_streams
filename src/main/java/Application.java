import first.FirstTest;
import third.ThirdTask;
import view.CommandRunner;
import view.User;

public class Application {

    public static void main(String[] args) {
        FirstTest ft = new FirstTest();
        ft.start();

        CommandRunner cr = new CommandRunner();
        cr.start();

        ThirdTask tt = new ThirdTask();
        tt.start();

        User user = User.getUser();
        user.start();
    }
}
