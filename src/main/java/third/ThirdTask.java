package third;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implements a few methods that returns list of random integers.
 * Count average, min, max, sum, count number of values that are bigger than average of list values.
 */
public class ThirdTask {

    private static Logger log = LogManager.getLogger(ThirdTask.class);

    /**
     * Method start.
     */
    public void start() {
        Random random = new Random();
        List<Integer> integerList = Stream.generate(() -> Integer.valueOf(random.nextInt(100)))
                .limit(20)
                .collect(Collectors.toList());
        OptionalDouble average = integerList.stream()
                .mapToInt(i -> i)
                .average();
        log.info("average = " + average.orElse(0));

        Optional<Integer> max = integerList.stream()
                .max(Integer::compareTo);
        log.info("max = " + max.orElse(0));

        int sum = integerList.stream()
                .mapToInt(i -> i)
                .sum();
        log.info("sum = " + sum);

        Integer sumReduce = integerList.stream()
                .reduce((a, b) -> a + b)
                .orElse(0);
        log.info("sumReduce = " + sumReduce);

        long count = integerList.stream()
                .filter(i -> i > average.getAsDouble())
                .count();
        log.info("count = " + count);
        System.out.println(integerList);
    }
}
