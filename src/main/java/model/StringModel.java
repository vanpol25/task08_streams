package model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class manipulate by data from scanner.
 * Can show statistic.
 * Class implements singleton pattern.
 *
 * @author Ivan Polahniuk
 */
public class StringModel {

    private static StringModel sc;
    private Logger log = LogManager.getLogger(StringModel.class);
    /**
     * Main field to store all data.
     */
    private List<String> list = new ArrayList<>();

    private StringModel() {

    }

    /**
     * @return - create if exists object {@link StringModel}.
     */
    public static StringModel getInstance() {
        if (sc == null) {
            sc = new StringModel();
        }
        return sc;
    }

    /**
     * Add new strings to list {@link StringModel#list}.
     * Also trim every string.
     * @param s
     */
    public void add(String s) {
        if (s == null || s.equals("")) {
            return;
        }
        list.add(s.trim());
        log.trace("added new String[" + s + ']');
    }

    /**
     * @return - return number of unique words in {@link StringModel#list}
     */
    public long getNumberOfUnique() {
        return list.stream()
                .distinct()
                .count();
    }

    /**
     * @return - unique strings in List.
     */
    public List<String> getSortedListOfUnique() {
        return list.stream().distinct().sorted(String::compareTo).collect(Collectors.toList());
    }

    /**
     * @return - map of unique strings of {@link StringModel#list} and there count as a value.
     */
    public Map<String, Long> getWordsNumber() {
        Map<String, Long> map = new HashMap<>();
        for (String s : getUnique(list)) {
            map.put(s, list.stream().filter(a -> a.equals(s)).count());
        }
        return map;
    }

    /**
     * @return - map of unique leters of strings of {@link StringModel#list} and there count as a value.
     */
    public Map<Character, Integer> getLettersNumber() {
        Map<Character, Integer> map = new HashMap<>();
        for (String s : list) {
            for (char c : s.toCharArray()) {
                map.merge(c, 1, Integer::sum);
            }
        }
        return map;
    }

    /**
     * @param stringList - list with different strings.
     * @return - list with unique strings.
     */
    private List<String> getUnique(List<String> stringList) {
        return stringList.stream().distinct().collect(Collectors.toList());
    }

    /**
     * @return - String that was joined with " " of all strings in {@link StringModel#list}
     */
    public String getText() {
        return list.stream().reduce((a, b) -> a + " " + b).orElse("");
    }
}
